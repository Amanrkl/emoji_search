import React, { Component } from 'react';
import Display from './display.js';
import '../assets/style/searchBar.css';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        };
    }


    handleChange = (event) => {
        this.setState({
            value: event.target.value
        });
    }

    render() {
        return (
            <div>
                <input 
                className="searchBar" 
                type="text" 
                placeholder="Type to search emojis" 
                onChange={this.handleChange} 
                />
                <Display searchText={this.state} />
            </div>

        );
    }
}

export default SearchBar;