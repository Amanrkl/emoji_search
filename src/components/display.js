import React, { Component } from 'react';
import EmojiCard from './card';
import '../assets/style/display.css';
import emojisData from '../assets/data/emojisData.json';

class Display extends Component {

    render() {
        let emojis = Object.entries(emojisData).slice(0, 484);

        if (this.props.searchText.value.trim() !== "") {
            emojis = Object
                .entries(emojisData)
                .filter(([emojiSymbol, emojiDetails]) => {
                    return (
                        emojiDetails.slug
                            .includes(this.props.searchText.value.toLowerCase().trim())
                        || emojiDetails.group
                            .toLowerCase()
                            .includes(this.props.searchText.value.toLowerCase().trim())
                        || emojiSymbol
                            .includes(this.props.searchText.value.toLowerCase().trim())
                    )
                })
        }

        return (
            <div className="emoji-container">
                {
                    (emojis.length)
                        ? emojis.map(([emojiSymbol, emojiDetails], index) => (
                            <EmojiCard
                                key={index}
                                symbol={emojiSymbol}
                                name={emojiDetails.name}
                            />
                        ))
                        : <p className="message">No emoji Found, Try different keyword</p>

                }
            </div>

        );
    }
}

export default Display;