import React, { Component } from 'react';
import '../assets/style/card.css';

class EmojiCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCopied: false,
            copyMessage: 'Copied',
        }
    }

    handleClick = () => {
        navigator.clipboard.writeText(this.props.symbol)
            .then(() => {
                console.log('Copied Successfully', this.props.name)
                this.setState(
                    {
                        isCopied: true,
                        copyMessage: 'Copied',
                    }, () => {
                        setTimeout(() => {
                            this.setState({
                                isCopied: false
                            })
                        }, 500)
                    }
                )


            })
            .catch((err) => {
                console.error(err);
                this.setState(
                    {
                        isCopied: true,
                        copyMessage: 'Can\'t copy',
                    }, () => {
                        setTimeout(() => {
                            this.setState({
                                isCopied: false
                            })
                        }, 1000)
                    }
                )
            })
    }

    render() {
        return (
            <div className="card" onClick={this.handleClick} >
                <span className="copyMessage">{(this.state.isCopied && this.state.copyMessage)}</span>
                <span className="item-emoji">{(!this.state.isCopied && this.props.symbol)}</span>
            </div>
        );
    }
}

export default EmojiCard;