import './App.css';
import SearchBar from './components/searchBar';

function App() {
  return (
    <div className="App">
      <h1>
        Emoji Search
      </h1>
      <SearchBar />
    </div>
  );
}

export default App;
